variable "access_key" {
  default = "XXXXXXXXXXXXXXXX"
}

variable "secret_key" {
  default = "YYYYYYYYYYYYYYYYY"
}

variable "region" {
  default = "ap-southeast-1"
}

variable "az1" {
  default = "ap-southeast-1a"
}

variable "az2" {
  default = "ap-southeast-1b"
}

variable "scenario" {
  default = "eks-cluster"
}

variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1y...qd4hssndQ== rsa-key-20180518"
}
