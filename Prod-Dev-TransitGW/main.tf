###############
# VPC Section #
###############

# VPCs

resource "aws_vpc" "vpc-1" {
  cidr_block = "10.10.0.0/16"
  tags = {
    Name = "${var.scenario}-vpc1-dev"
    scenario = "${var.scenario}"
    env = "dev"
  }
}

resource "aws_vpc" "vpc-2" {
  cidr_block = "10.12.0.0/16"
  tags = {
    Name = "${var.scenario}-vpc2-shared"
    scenario = "${var.scenario}"
    env = "shared"
  }
}

resource "aws_vpc" "vpc-3" {
  cidr_block = "10.13.0.0/16"
  tags = {
    Name = "${var.scenario}-vpc3-prod"
    scenario = "${var.scenario}"
    env = "prod"
  }
}

# Subnets

resource "aws_subnet" "vpc-1-sub-a" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.10.1.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-a"
  }
}

resource "aws_subnet" "vpc-1-sub-b" {
  vpc_id     = "${aws_vpc.vpc-1.id}"
  cidr_block = "10.10.2.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-1.tags.Name}-sub-b"
  }
}

resource "aws_subnet" "vpc-2-sub-a" {
  vpc_id     = "${aws_vpc.vpc-2.id}"
  cidr_block = "10.12.1.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-2.tags.Name}-sub-a"
  }
}

resource "aws_subnet" "vpc-2-sub-b" {
  vpc_id     = "${aws_vpc.vpc-2.id}"
  cidr_block = "10.12.2.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-2.tags.Name}-sub-b"
  }
}

resource "aws_subnet" "vpc-3-sub-a" {
  vpc_id     = "${aws_vpc.vpc-3.id}"
  cidr_block = "10.13.1.0/24"
  availability_zone = "${var.az1}"

  tags = {
    Name = "${aws_vpc.vpc-3.tags.Name}-sub-a"
  }
}

resource "aws_subnet" "vpc-3-sub-b" {
  vpc_id     = "${aws_vpc.vpc-3.id}"
  cidr_block = "10.13.2.0/24"
  availability_zone = "${var.az2}"

  tags = {
    Name = "${aws_vpc.vpc-3.tags.Name}-sub-b"
  }
}

# Internet Gateway

resource "aws_internet_gateway" "vpc-2-igw" {
  vpc_id = "${aws_vpc.vpc-2.id}"

  tags = {
    Name = "vpc-2-igw"
    scenario = "${var.scenario}"
  }
}

# Main Route Tables Associations

resource "aws_main_route_table_association" "main-rt-vpc-1" {
  vpc_id         = "${aws_vpc.vpc-1.id}"
  route_table_id = "${aws_route_table.vpc-1-rtb.id}"
}

resource "aws_main_route_table_association" "main-rt-vpc-2" {
  vpc_id         = "${aws_vpc.vpc-2.id}"
  route_table_id = "${aws_route_table.vpc-2-rtb.id}"
}

resource "aws_main_route_table_association" "main-rt-vpc-3" {
  vpc_id         = "${aws_vpc.vpc-3.id}"
  route_table_id = "${aws_route_table.vpc-3-rtb.id}"
}


# Route Tables
resource "aws_route_table" "vpc-1-rtb" {
  vpc_id = "${aws_vpc.vpc-1.id}"

  route {
    cidr_block = "10.0.0.0/8"
    transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  }

  tags = {
    Name       = "vpc-1-rtb"
    env        = "dev"
    scenario = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

resource "aws_route_table" "vpc-2-rtb" {
  vpc_id = "${aws_vpc.vpc-2.id}"

  route {
    cidr_block = "10.0.0.0/8"
    transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.vpc-2-igw.id}"
  }

  tags = {
    Name       = "vpc-2-rtb"
    env        = "shared"
    scenario = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

resource "aws_route_table" "vpc-3-rtb" {
  vpc_id = "${aws_vpc.vpc-3.id}"

  route {
    cidr_block = "10.0.0.0/8"
    transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  }

  tags = {
    Name       = "vpc-3-rtb"
    env        = "prod"
    scenario = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}


###########################
# Transit Gateway Section #
###########################

# Transit Gateway
resource "aws_ec2_transit_gateway" "tgw" {
  description                     = "Transit Gateway scenario with 3 VPCs, 2 subnets each"
  default_route_table_association = "disable"
  default_route_table_propagation = "disable"
  tags                            = {
    Name                          = "${var.scenario}"
    scenario                      = "${var.scenario}"
  }
}

# VPC attachment

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-1" {
  subnet_ids         = ["${aws_subnet.vpc-1-sub-a.id}", "${aws_subnet.vpc-1-sub-b.id}"]
  transit_gateway_id = "${aws_ec2_transit_gateway.test-tgw.id}"
  vpc_id             = "${aws_vpc.vpc-1.id}"
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
  tags               = {
    Name             = "tgw-att-vpc1"
    scenario         = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-2" {
  subnet_ids         = ["${aws_subnet.vpc-2-sub-a.id}", "${aws_subnet.vpc-2-sub-b.id}"]
  transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  vpc_id             = "${aws_vpc.vpc-2.id}"
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
  tags               = {
    Name             = "tgw-att-vpc2"
    scenario         = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-3" {
  subnet_ids         = ["${aws_subnet.vpc-3-sub-a.id}", "${aws_subnet.vpc-3-sub-b.id}"]
  transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  vpc_id             = "${aws_vpc.vpc-3.id}"
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
  tags               = {
    Name             = "tgw-att-vpc3"
    scenario         = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

# Route Tables

resource "aws_ec2_transit_gateway_route_table" "tgw-dev-rt" {
  transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  tags               = {
    Name             = "tgw-dev-rt"
    scenario         = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

resource "aws_ec2_transit_gateway_route_table" "tgw-shared-rt" {
  transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  tags               = {
    Name             = "tgw-shared-rt"
    scenario         = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

resource "aws_ec2_transit_gateway_route_table" "tgw-prod-rt" {
  transit_gateway_id = "${aws_ec2_transit_gateway.tgw.id}"
  tags               = {
    Name             = "tgw-prod-rt"
    scenario         = "${var.scenario}"
  }
  depends_on = ["aws_ec2_transit_gateway.tgw"]
}

# Route Tables Associations

resource "aws_ec2_transit_gateway_route_table_association" "tgw-rt-vpc-1-assoc" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-1.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-dev-rt.id}"
}

resource "aws_ec2_transit_gateway_route_table_association" "tgw-rt-vpc-2-assoc" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-2.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-shared-rt.id}"
}

resource "aws_ec2_transit_gateway_route_table_association" "tgw-rt-vpc-3-assoc" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-3.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-prod-rt.id}"
}

# Route Tables Propagations

resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-dev-to-vpc-1" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-1.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-dev-rt.id}"
}

resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-dev-to-vpc-2" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-2.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-dev-rt.id}"
}
resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-dev-to-vpc-3" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-3.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-dev-rt.id}"
}

resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-shared-to-vpc-1" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-1.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-shared-rt.id}"
}

resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-shared-to-vpc-3" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-4.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-shared-rt.id}"
}

resource "aws_ec2_transit_gateway_route_table_propagation" "tgw-rt-prod-to-vpc-2" {
  transit_gateway_attachment_id  = "${aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-2.id}"
  transit_gateway_route_table_id = "${aws_ec2_transit_gateway_route_table.tgw-prod-rt.id}"
}

#########################
# EC2 Instances Section #
#########################

# Key Pair

resource "aws_key_pair" "tgw-keypair" {
  key_name   = "tgw-keypair"
  public_key = "${var.public_key}"
}

# Security Groups

resource "aws_security_group" "sec-group-vpc-1-ssh-icmp" {
  name        = "sec-group-vpc-1-ssh-icmp"
  description = "test-tgw: Allow SSH and ICMP traffic"
  vpc_id      = "${aws_vpc.vpc-1.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8 # the ICMP type number for 'Echo'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0 # the ICMP type number for 'Echo Reply'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec-group-vpc-1-ssh-icmp"
    scenario = "${var.scenario}"
  }
}

resource "aws_security_group" "sec-group-vpc-2-ssh-icmp" {
  name        = "sec-group-vpc-2-ssh-icmp"
  description = "test-tgw: Allow SSH and ICMP traffic"
  vpc_id      = "${aws_vpc.vpc-2.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8 # the ICMP type number for 'Echo'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0 # the ICMP type number for 'Echo Reply'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec-group-vpc-2-ssh-icmp"
    scenario = "${var.scenario}"
  }
}

resource "aws_security_group" "sec-group-vpc-3-ssh-icmp" {
  name        = "sec-group-vpc-3-ssh-icmp"
  description = "test-tgw: Allow SSH and ICMP traffic"
  vpc_id      = "${aws_vpc.vpc-3.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8 # the ICMP type number for 'Echo'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0 # the ICMP type number for 'Echo Reply'
    to_port     = 0 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec-group-vpc-3-ssh-icmp"
    scenario = "${var.scenario}"
  }
}